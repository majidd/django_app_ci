#!/usr/bin/env bash

#export DOCKER_HOST="/var/run/docker/containerd/containerd.sock"
#docker-machine create --driver virtualbox dev
#docker-machine ls
docker-compose --verbose build
docker-compose up
