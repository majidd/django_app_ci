# A Simple Test-Case for CI/CD 
This is a simple Django web project in order to test CI/CD pipeline
# How it works
Every changes made to master branch triggered the CI pipeline which defined in the .gitlab-ci.yml file. Currently it has 3 stages and can extend to any number of desire stages.
# The Runners
We can use gitlab shared runners in order to accepts the jobs and runs them. Another option is provided which is a specific runner. It's installed in my laptop and configured to acceptes jobs only from this repo.
# Build Stage
In the build stage, by using docker as the executer and docker:docker-in-docker(DinD) as service, the image is built. If the we don't configure docker registry for that service, it will use Dokerfile to build  from scratch each time the pipeline starts. When the number of conainers grow up, it takes serveral time to build!
After the build is done, the image will be taged and pushed to the container registery in the current gitlab repo. We can configure this docker registery to DinD service.
# Test Stage
The written unittests are runned in this stage and coverage report printed as well. 